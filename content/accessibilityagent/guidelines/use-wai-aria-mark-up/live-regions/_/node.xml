<node xmlns="urn:enonic:xp:export:1.0">
    <id>7cfa6a4b-7b44-4f88-8631-ec337be5688d</id>
    <childOrder>modifiedtime DESC</childOrder>
    <nodeType>content</nodeType>
    <data>
        <boolean name="valid">true</boolean>
        <string name="displayName">Live regions</string>
        <string name="type">com.enonic.module.accessibilityagent:codeblock</string>
        <string name="owner">user:system:su</string>
        <dateTime name="modifiedTime">2015-05-18T18:55:39.802Z</dateTime>
        <string name="modifier">user:system:su</string>
        <string name="creator">user:system:su</string>
        <dateTime name="createdTime">2015-05-18T18:42:09.709Z</dateTime>
        <property-set name="data">
            <property-set name="codeblock">
                <property-set name="info">
                    <string name="heading"/>
                    <htmlPart name="preface">&lt;p&gt;The aria-live&amp;nbsp;property has a value indicating one of three verbosity levels in a region:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;Off: This indicates that the region is not live&lt;/li&gt;
&lt;li&gt;Polite: This indicates that it is not necessary to respond until user completes their current activity&lt;/li&gt;
&lt;li&gt;Assertive: This value is a higher priority than normal, but does not necessarily interrupt the user immediately&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;They are written as such:&lt;/p&gt;</htmlPart>
                </property-set>
                <htmlPart name="code">&lt;ul aria-live="off"&gt;
&lt;ul aria-live="polite"&gt;
&lt;ul aria-live="assertive"&gt;</htmlPart>
                <string name="codetype">html</string>
                <htmlPart name="appendix"/>
                <property-set name="config">
                    <string name="lineNumbers">nil</string>
                    <string name="highlightLines"/>
                    <string name="lineNumberAnchorsPrefix"/>
                    <string isNull="true" name="css"/>
                    <string isNull="true" name="breakLines"/>
                    <string isNull="true" name="hint"/>
                    <string isNull="true" name="wrap"/>
                    <long isNull="true" name="tabWidth"/>
                </property-set>
            </property-set>
            <property-set name="codeblock">
                <property-set name="info">
                    <string name="heading"/>
                    <htmlPart name="preface">&lt;p&gt;Other important properties that can be used when defining live regions are:&lt;/p&gt;
&lt;p&gt;&lt;em&gt;Aria-atomic&lt;/em&gt;: Indicates if AT should present all or only part of the changed region to the user. It has the values true or false. If this property is set to true, AT should present the entire region as a whole.&lt;/p&gt;
&lt;p&gt;In the following example, all elements within an unordered list will be announced in their entirety when the region is spoken, unless another element further down the chain overrides the&amp;nbsp;aria-atomic&amp;nbsp;property.&lt;/p&gt;</htmlPart>
                </property-set>
                <htmlPart name="code">&lt;ul aria-atomic="true"
    aria-live="polite"&gt;</htmlPart>
                <string name="codetype">html</string>
                <htmlPart name="appendix"/>
                <property-set name="config">
                    <string name="lineNumbers">nil</string>
                    <string name="highlightLines"/>
                    <string name="lineNumberAnchorsPrefix"/>
                    <string isNull="true" name="css"/>
                    <string isNull="true" name="breakLines"/>
                    <string isNull="true" name="hint"/>
                    <string isNull="true" name="wrap"/>
                    <long isNull="true" name="tabWidth"/>
                </property-set>
            </property-set>
            <property-set name="codeblock">
                <property-set name="info">
                    <string name="heading"/>
                    <htmlPart name="preface">&lt;p&gt;&lt;em&gt;Aria-busy&lt;/em&gt;: Prevents AT announcing changes before the updates are complete. It has the values &lt;em&gt;true&lt;/em&gt; or &lt;em&gt;false&lt;/em&gt;. If multiple parts of a live region need to be loaded before changes are announced to the user, the aria-busy property can be set to true until the final part is loaded, and then set to false when the updates are complete.&lt;/p&gt;</htmlPart>
                </property-set>
                <htmlPart name="code">&lt;ul aria-atomic="true"
    aria-live="polite"
    aria-busy="true"&gt;</htmlPart>
                <string name="codetype">html</string>
                <htmlPart name="appendix"/>
                <property-set name="config">
                    <string name="lineNumbers">nil</string>
                    <string name="highlightLines"/>
                    <string name="lineNumberAnchorsPrefix"/>
                    <string isNull="true" name="css"/>
                    <string isNull="true" name="breakLines"/>
                    <string isNull="true" name="hint"/>
                    <string isNull="true" name="wrap"/>
                    <long isNull="true" name="tabWidth"/>
                </property-set>
            </property-set>
            <property-set name="codeblock">
                <property-set name="info">
                    <string name="heading"/>
                    <htmlPart name="preface">&lt;p&gt;&lt;em&gt;Aria-relevant&lt;/em&gt;: Indicates what changes are considered relevant within a region. Accepts a space separated list of the following property values:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;&lt;em&gt;Additions&lt;/em&gt;: Nodes are added to the DOM within the region.&lt;/li&gt;
&lt;li&gt;&lt;em&gt;Removals&lt;/em&gt;: Nodes are removed from the DOM within the region.&lt;/li&gt;
&lt;li&gt;&lt;em&gt;Text&lt;/em&gt;: Text is added or removed from the DOM.&lt;/li&gt;
&lt;li&gt;&lt;em&gt;All&lt;/em&gt;: All of the above apply to this region.&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;In the absence of an explicit&amp;nbsp;aria-relevant&amp;nbsp;property, the default is to assume there are text changes and additions:&lt;/p&gt;</htmlPart>
                </property-set>
                <htmlPart name="code">&lt;ul aria-relevant="text additions"&gt;</htmlPart>
                <string name="codetype">html</string>
                <htmlPart name="appendix"/>
                <property-set name="config">
                    <string name="lineNumbers">nil</string>
                    <string name="highlightLines"/>
                    <string name="lineNumberAnchorsPrefix"/>
                    <string isNull="true" name="css"/>
                    <string isNull="true" name="breakLines"/>
                    <string isNull="true" name="hint"/>
                    <string isNull="true" name="wrap"/>
                    <long isNull="true" name="tabWidth"/>
                </property-set>
            </property-set>
            <property-set name="codeblock">
                <property-set name="info">
                    <string name="heading"/>
                    <htmlPart name="preface">&lt;p&gt;The following example would only announce changes if nodes are added to the DOM within the region. If there are text changes, or nodes are removed within the region, the user will not be notified.&lt;/p&gt;</htmlPart>
                </property-set>
                <htmlPart name="code">&lt;ul aria-relevant="additions"
    aria-atomic="true"
    aria-live="polite"&gt;</htmlPart>
                <string name="codetype">html</string>
                <htmlPart name="appendix"/>
                <property-set name="config">
                    <string name="lineNumbers">nil</string>
                    <string name="highlightLines"/>
                    <string name="lineNumberAnchorsPrefix"/>
                    <string isNull="true" name="css"/>
                    <string isNull="true" name="breakLines"/>
                    <string isNull="true" name="hint"/>
                    <string isNull="true" name="wrap"/>
                    <long isNull="true" name="tabWidth"/>
                </property-set>
            </property-set>
        </property-set>
        <property-set name="x">
            <property-set name="com-enonic-module-accessibilityagent">
                <property-set name="show-in-menu">
                    <boolean name="menuItem">false</boolean>
                    <string name="menuName"/>
                </property-set>
            </property-set>
        </property-set>
        <property-set name="page">
            <string isNull="true" name="controller"/>
            <reference name="template">054cfdba-8ac3-443f-901f-fe3d1372e771</reference>
        </property-set>
    </data>
    <indexConfigs>
        <analyzer>content_default</analyzer>
        <defaultConfig>
            <decideByType>true</decideByType>
            <enabled>true</enabled>
            <nGram>false</nGram>
            <fulltext>false</fulltext>
            <includeInAllText>false</includeInAllText>
        </defaultConfig>
        <pathIndexConfigs>
            <pathIndexConfig>
                <indexConfig>
                    <decideByType>false</decideByType>
                    <enabled>false</enabled>
                    <nGram>false</nGram>
                    <fulltext>false</fulltext>
                    <includeInAllText>false</includeInAllText>
                </indexConfig>
                <path>page.regions</path>
            </pathIndexConfig>
            <pathIndexConfig>
                <indexConfig>
                    <decideByType>false</decideByType>
                    <enabled>true</enabled>
                    <nGram>false</nGram>
                    <fulltext>false</fulltext>
                    <includeInAllText>false</includeInAllText>
                </indexConfig>
                <path>attachment</path>
            </pathIndexConfig>
            <pathIndexConfig>
                <indexConfig>
                    <decideByType>false</decideByType>
                    <enabled>true</enabled>
                    <nGram>false</nGram>
                    <fulltext>false</fulltext>
                    <includeInAllText>false</includeInAllText>
                </indexConfig>
                <path>x</path>
            </pathIndexConfig>
            <pathIndexConfig>
                <indexConfig>
                    <decideByType>true</decideByType>
                    <enabled>true</enabled>
                    <nGram>false</nGram>
                    <fulltext>false</fulltext>
                    <includeInAllText>false</includeInAllText>
                </indexConfig>
                <path>data</path>
            </pathIndexConfig>
            <pathIndexConfig>
                <indexConfig>
                    <decideByType>false</decideByType>
                    <enabled>true</enabled>
                    <nGram>false</nGram>
                    <fulltext>false</fulltext>
                    <includeInAllText>false</includeInAllText>
                </indexConfig>
                <path>page</path>
            </pathIndexConfig>
            <pathIndexConfig>
                <indexConfig>
                    <decideByType>false</decideByType>
                    <enabled>false</enabled>
                    <nGram>false</nGram>
                    <fulltext>false</fulltext>
                    <includeInAllText>false</includeInAllText>
                </indexConfig>
                <path>site</path>
            </pathIndexConfig>
            <pathIndexConfig>
                <indexConfig>
                    <decideByType>false</decideByType>
                    <enabled>true</enabled>
                    <nGram>false</nGram>
                    <fulltext>false</fulltext>
                    <includeInAllText>false</includeInAllText>
                </indexConfig>
                <path>type</path>
            </pathIndexConfig>
        </pathIndexConfigs>
    </indexConfigs>
</node>
